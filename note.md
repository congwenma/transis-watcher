## Ran into `define` is not defined error
Fixed by adding this before `transisReact.dist.js`
```js
  if (typeof define === 'function' && define.amd) {
    var define = function(factory) { module.exports = factory(require)}
  }
```


## WIP
- [] animation on change of props
- [] show stack trace when props change
- [] use a watch mode (eye icon) for when props change