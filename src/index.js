import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Transis from 'transis'

// had to add a `define` polyfill for this
import transisReact from './transisReact.dist'
// var TransisReact = require('./transis-react.dist')

window.transisReact = transisReact
const Origin = Transis.Model.extend('Origin', function () {
  this.attr('name', 'string')
  this.hasMany('people', 'Person')
})

const Person = Transis.Model.extend('Person', function () {
  this.attr('firstName', 'string', { default: 'John' })
  this.attr('lastName', 'string', { default: 'Doe' })
  this.prop('fullName', {
    on: ['firstName', 'lastName'],
    get: (fn, ln) => `${fn} ${ln}`
  })
  this.prop('title', {
    on: ['fullName', 'origin'],
    get: (fullName, origin) => `${fullName} of ${origin.name}`
  })
  this.hasOne('origin', 'Origin')
})

window.Person = Person
window.Origin = Origin

const toJSONNoCircular = (model, name) => {
  return JSON.stringify(model[name], (key, value) => {
    if (value === model) {
      return 'self';
    }
    else if(value instanceof Transis.Model) {
      return `<${value.constructor.displayName}>: ${value.id || value.name}`
    }
    return value;
  })
}


class TransisWatcher extends React.Component {
  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
    // You can also log the error to an error reporting service
    console.error(error, info);
  }

  render() {
    const { model } = this.props
    // debugger
    if (model == null) return null

    const tProps = Object.entries(
      model.constructor.prototype.__props__
    )

    return <div className="TransisWatcher">
      <h3>Transis Watcher</h3>
      <div className="transis-model">
        <ul className="prop-list">
        {tProps.map(tProp => {
          const [name, config] = tProp
          const value = toJSONNoCircular(model, name)
          return <li style={{ wordBreak: 'break-word' }}>
            {/* { name } - { JSON.stringify(config) } */}
            {name}: {value}
          </li>
        })}
        </ul>
      </div>
    </div>
  }
}

const Wrapper = transisReact({
  props: {
    model: ['*']
  }
}, TransisWatcher)

const globalObj = new (Transis.Object.extend(function() {
  this.prop('person1')
  this.prop('person2')
}))

// NOTE: default
const person1 = globalObj.person1 = new Person({
  origin: new Origin({ name: "Foo" })
});

const person2 = globalObj.person2 = new Person({
  firstName: 'Joe',
  lastName: 'Smith',
  origin: new Origin({ name: "Bar" })
});

class App extends React.Component {
  constructor() {
    super()
    this.state = { input: 'person1', pendingInput: '' }
  }
  setInput = () => {
    const { pendingInput } = this.state
    if (!globalObj[pendingInput]) {
      // TODO: setError here
      return null
    }
    this.setState({
      input: pendingInput,
      pendingInput: ''
    })
  }

  render() {
    const { input } = this.state
    const AwareComponent = transisReact(
      {
        global: globalObj,
        state: {
          // NOTE: single object that we should be able to change
          [input]: ['*']
        }
      },
      (props) => <div className="AwareComponent">
        <input
          onChange={
            e => this.setState({ pendingInput: e.target.value })
          }
          value={input}
        />
        <button onClick={this.setInput}>Load model</button>
        {/* NOTE: getting the model */}
        <Wrapper model={props[input]} />
      </div>
    )
    return <div>
      <AwareComponent input={input}/>
      {/* NOTE: actual app */}
      {
        Object.entries({person1, person2}).map((modelInfo, index) =>
          <ModelRender
            modelInfo={modelInfo}
            key={index}
            handleSelect={() => {
              this.state.pendingInput = modelInfo[0]
              this.setInput()
            }}
          />
        )
      }
    </div>
  }
}

const ModelRender = ({ modelInfo, handleSelect }) => {
  const [modelKey, model] = modelInfo
  const tProps = Object.entries(model.constructor.prototype.__props__)

  return <div className="ModelRender">
    <h3>
      Model Render: <b style={{ textDecoration: 'underline' }}>{modelKey}</b> -
      {`<${model.constructor.displayName}>`}
      <button onClick={handleSelect}>Load</button>
    </h3>
    <div className="transis-model">
      <ul className="prop-list">
        {tProps.map((tProp, index) => {
          const [name, config] = tProp
          const value = toJSONNoCircular(model, name)
          return <li style={{ wordBreak: 'break-word' }} key={index}>
            {name}: {value}
          </li>
        })}
      </ul>
    </div>
  </div>
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)

registerServiceWorker();
